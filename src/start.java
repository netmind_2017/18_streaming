import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import classes.Person;

public class start {

	public start() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
	String[] line;
		
		try {
			   //input
			   File file = new File("import.txt");
			   Scanner scanner = new Scanner(file);
			   	  
			   while (scanner.hasNext()) {
				   line = scanner.next().split(";");
				   Person x = new Person(line[0], line[1], line[2]);
				   if (x.isAdult()== false) {
					   System.out.println(x.name +  " " + x.surname + " is not adult");
				   }
				 
			   }
			   
			   scanner.close();
			   
		} catch (FileNotFoundException e) {
			 System.out.println("ERROR: " + e.getMessage());
		} 
		

	}

}
