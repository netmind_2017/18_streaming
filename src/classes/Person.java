package classes;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Person {
	
	public String name;
	public String surname;
	private String dateofbirth;
	private long diffDays;
	public int age;
	
	public Person(String name,String surname, String dateofbirth ) {
		this.name = name;
		this.surname = surname;
		this.dateofbirth= dateofbirth;
		this.age = AgeCalculator();
	}
	
	private Integer AgeCalculator() {
		
		//CURRENT DATE
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		Date currentDate = new Date();
		//System.out.println("Current Date : "+dateFormat.format(currentDate));
		
		//CONVERT DATE
		Date dateToCheck = null;
		try {
			dateToCheck = dateFormat.parse(this.dateofbirth);
			//DAY DIFF
			this.diffDays = Math.abs(currentDate.getTime() - dateToCheck.getTime());
			//System.out.println("differents in millisecond  : " + this.diffDays );
			//System.out.println("num of days  : " + this.diffDays  / (1000 * 60 * 60 * 24));
			
			this.diffDays = (this.diffDays / (1000 * 24 * 60 * 60 ) / 365);
			this.age = (int) this.diffDays;
			//System.out.println("Age :" + this.diffDays);	
			return this.age;
			
		} catch (ParseException e) {
			System.out.println("data" +  this.dateofbirth +  " is wronge");
			return 0;
		}

		
	}
	
	public boolean isAdult() {
		if(this.age >= 18){
			return true;
		}else{
			return false;
		}
	}
		
}